(function() {

	// Ref:
	// https://coolaj86.com/articles/webcrypto-encrypt-and-decrypt-with-aes/
	// https://coolaj86.com/articles/typedarray-buffer-to-base64-in-javascript/

	let CryptoSuite = {

		ivLen : 16,

		initialize : function()
		{
			return this;
		},

		isCryptoSupported : function()
		{
			return window.crypto != undefined;
		},

		generateUUIDV4 : function()
		{
			return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
				(c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
			);
		},

		convertStringToArrayBuffer : function(string)
		{
			var bytes = new Uint8Array(string.length);

			for (var iii = 0; iii < string.length; iii++)
				bytes[iii] = string.charCodeAt(iii);

			return bytes;
		},

		convertArrayBufferToString : function(buffer)
		{
			var string = "";

			for (var iii = 0; iii < buffer.byteLength; iii++)
				string += String.fromCharCode(buffer[iii]);

			return string;
		},

		generateKeyForUUID : function(uuid)
		{
			let arrayBuffer = this.convertStringToArrayBuffer(uuid);

			return crypto.subtle.digest({name: "SHA-256"}, arrayBuffer)
				.then(function(result){

				return crypto.subtle.importKey(
					"raw",
					result,
					{name: "AES-CBC"},
					false,
					["encrypt", "decrypt"])
					.finally(function(e){
						return e;
						}, function(e){
							console.log(e);
						}
					 );
				}
			);
		},

		joinIvAndData : function(iv, data)
		{
			var buf = new Uint8Array(iv.length + data.length);
			Array.prototype.forEach.call(iv, (byte, i) => {
				buf[i] = byte;
			});
			Array.prototype.forEach.call(data, (byte, i) => {
				buf[this.ivLen + i] = byte;
			});
			return buf;
		},

		bufferToBase64: function(buf)
		{
			var binstr = Array.prototype.map.call(buf, function (ch) {
				return String.fromCharCode(ch);
			}).join('');
			return btoa(binstr);
		},

		base64ToBuffer: function(base64)
		{
			var binstr = atob(base64);
			var buf = new Uint8Array(binstr.length);
			Array.prototype.forEach.call(binstr, function (ch, i) {
				buf[i] = ch.charCodeAt(0);
			});
			return buf;
		},

		separateIvFromData: function (buf)
		{
			var iv = new Uint8Array(this.ivLen);
			var data = new Uint8Array(buf.length - this.ivLen);
			Array.prototype.forEach.call(buf, (byte, i) => {
				if (i < this.ivLen) {
					iv[i] = byte;
				} else {
					data[i - this.ivLen] = byte;
				}
			});
			return { iv: iv, data: data };
		},

		encrypt : function(contents, uuidKey)
		{
			return this.generateKeyForUUID(uuidKey)
				.then((key) => {

					let initializationVector = new Uint8Array(this.ivLen);

					crypto.getRandomValues(initializationVector);

					let data = this.convertStringToArrayBuffer(contents);

					return crypto.subtle.encrypt(
						{ name: 'AES-CBC', iv: initializationVector }
						, key
						, data
					).then((encrypted) => {

						let encryptedData = new Uint8Array(encrypted);
						let ciphered = this.joinIvAndData(initializationVector, encryptedData)

						return this.bufferToBase64(ciphered);
					});
			});
		},

		decrypt: function(encrypted, uuidKey)
		{
			let buffer = this.base64ToBuffer(encrypted);
			var parts = this.separateIvFromData(buffer);

			return this.generateKeyForUUID(uuidKey)
				.then((key) => {

					return crypto.subtle.decrypt(
						{ name: 'AES-CBC', iv: parts.iv }
						, key
						, parts.data
						).then((decrypted) => {

						let buffer = new Uint8Array(decrypted);
						let base64 = this.bufferToBase64(buffer);

						let string = atob(base64);

						return string;

					}, function(error)
					{
						console.log(error);
					});
				});
		}
	}

	let Inspector = {

		inspectorElement : null,
		hovering : false,

		initialize : function(inspectorElement)
		{
			this.inspectorElement = inspectorElement;

			this.inspectorElement.querySelector('h5 em').addEventListener('click', this.hide.bind(this));

			this.inspectorElement.addEventListener('mouseenter', () => {
				this.hovering = true;
			});

			this.inspectorElement.addEventListener('mouseleave', () => {
				this.hovering = false;
			});

			return this;
		},

		setHeader : function(header)
		{
			this.inspectorElement.querySelector('h5 span').innerText = header;
		},

		setInnerHTML : function(innerHTML)
		{
			this.inspectorElement.querySelector('.body').innerHTML = innerHTML;
		},

		reset : function()
		{
			this.hovering = false;
			this.inspectorElement.querySelector('.body').innerHTML = '';

			this.hide();
		},

		hide : function()
		{
			this.hovering = false;
			this.inspectorElement.className = 'close';
		},

		show : function()
		{
			this.inspectorElement.className = 'open';
		}
	}

	let Blackboard = {

		blackboardElement : null,
		blackboardButton : null,
		hovering : false,

		initialize : function(blackboardElement, blackboardButton)
		{
			this.blackboardElement = blackboardElement;
			this.blackboardButton = blackboardButton;

			this.blackboardElement.addEventListener('mouseenter', () => {
				this.hovering = true;
			});

			this.blackboardElement.addEventListener('mouseleave', () => {
				this.hovering = false;
			});
			this.blackboardButton.addEventListener('click', (e) => {

				e.preventDefault();

				if(this.blackboardButton.disabled)
					return;

				this.toggle();
			});

			return this;
		},

		reset : function()
		{
			this.blackboardButton.setAttribute('disabled', true);

			this.hovering = false;
			this.blackboardElement.querySelector('h5').innerHTML = '';
			this.blackboardElement.querySelector('ul').innerHTML = '';

			this.hide();
		},

		hide : function()
		{
			this.hovering = false;
			this.blackboardButton.dataset['opened'] = '0';
			this.blackboardElement.style.display = 'none';
		},

		show : function()
		{
			this.blackboardButton.dataset['opened'] = '1';
			this.blackboardElement.style.display = 'block';

			document.getElementById('library').className = '';
			document.getElementById('library-container').className ='close';
		},


		toggle : function()
		{
			let existingDisplay = this.blackboardElement.style.display;

			if(existingDisplay == 'block')
				this.hide();
			else
				this.show();
		}
	}

	let PannerDragger = {

		pageFocused: true,
		containerElement : null,
		blackboard: null,
		draggedNode : null,
		inspector: null,
		zoomElement : null,

		scaleFactor : 1.0,
		scaleStep : 0.1,
		minScaleFactor : 0.3,
		maxScaleFactor : 5.0,
		startScrollPosition : null,
		mouseDown : false,

		translateX : 0,
		translateY : 0,
		lastTranslateX : 0,
		lastTranslateY : 0,

		mouseX : 0,
		mouseY : 0,
		originX : 0,
		originY : 0,
		translateOriginX: 0,
		translateOriginY: 0,

		shaderLoaded: false,
		isEmbedded : false,
		isHoveringHeader : false,
		isPresenting: false,

		initialize : function(containerElement, zoomElement, blackboard, inspector)
		{
			this.blackboard = blackboard;
			this.inspector = inspector;
			this.zoomElement = zoomElement;
			this.containerElement = containerElement;
			this.isEmbedded = (document.body.className == 'page-embed');

			document.body.dataset['panning'] = 0;
			document.body.dataset['dragging'] = 0;

			window.addEventListener('focus', () => {
				this.pageFocused = true;
			});
			window.addEventListener('blur', () => {
				this.pageFocused = false;
			});

			let header = document.querySelector('body > header');

			header.addEventListener('mouseenter', (event) => {
				this.isHoveringHeader = true;
			});
			header.addEventListener('mouseleave', (event) => {
				this.isHoveringHeader = false;
			});

			document.addEventListener('mousedown', (event) => {
				this.panStarted(event);
			});

			document.addEventListener('mousemove', (event) => {
				this.panChanged(event);
			});

			document.addEventListener('mouseup', (event) => {
				this.panEnded(event);
			});

			window.addEventListener('wheel', (event) => {
				let resized = this.resize(event.deltaY, event.clientX, event.clientY);

				if(resized)
					event.preventDefault();
			});

			document.addEventListener('mousemove', this.onDragChanged.bind(this));
			document.addEventListener('mouseup', this.onDragEnded.bind(this));

			return this;
		},

		isPanGestureValid : function(event)
		{
			// For embedded graphs, we only accept left mouse click
			if(this.isEmbedded)
			{
				if(event.which == 2)
				{
					// Don't allow any scrolling via the middle click
					// for embedded graphs.
					event.preventDefault();
				}

				return event.which == 1;
			}

			// For not embedded graphs, either left or middle clicks
			// will do.
			return event.which == 1 || event.which == 2;
		},

		panStarted : function(event)
		{
			if(!this.shaderLoaded)
				return;

			if(this.draggedNode != null)
				return;

			if(!this.isPanGestureValid(event))
				return;

			if(this.blackboard.hovering
				|| this.inspector.hovering
				|| this.isHoveringHeader
				|| this.isPresenting)
				return;

			this.mouseDown = true;

			document.body.dataset['panning'] = 1;

			this.startScrollPosition = {
				x: event.screenX,
				y: event.screenY
			};

			event.preventDefault();
		},

		panChanged : function(event)
		{
			if(this.draggedNode != null)
				return;

			if(!this.isPanGestureValid(event))
				return;

			if(this.startScrollPosition == null)
				return;

			if(!this.mouseDown)
				return;

			this.translateX = this.lastTranslateX - (this.startScrollPosition.x - event.screenX) / this.scaleFactor;
			this.translateY = this.lastTranslateY - (this.startScrollPosition.y - event.screenY) / this.scaleFactor;

			this.updateTransform();
		},

		panEnded : function(event)
		{
			if(this.draggedNode != null)
				return;

			this.mouseDown = false;
			this.lastTranslateX = this.translateX;
			this.lastTranslateY = this.translateY;

			document.body.dataset['panning'] = 0;
		},

		resize : function(delta, x, y)
		{
			if(!this.pageFocused)
				return false;

			if(this.blackboard.hovering
				|| this.inspector.hovering
				|| this.isHoveringHeader
				|| this.isPresenting)
				return false;

			if(!this.shaderLoaded)
				return false;

			let oldScale = this.scaleFactor;
			let zoom = Math.exp((delta < 0 ? 1 : -1) * this.scaleStep)
			this.scaleFactor = this.scaleFactor * zoom;

			if(this.scaleFactor < this.minScaleFactor)
				this.scaleFactor = this.minScaleFactor;
			else if(this.scaleFactor > this.maxScaleFactor)
				this.scaleFactor = this.maxScaleFactor;

			let oldMouseX = this.mouseX;
			let oldMouseY = this.mouseY;

			this.mouseX = (x - this.containerElement.offsetLeft);
			this.mouseY = (y - this.containerElement.offsetTop);

			this.originX += ((this.mouseX - oldMouseX) / oldScale);
			this.originY += ((this.mouseY - oldMouseY) / oldScale);

			this.translateOriginX = (this.mouseX - this.originX) / this.scaleFactor;
			this.translateOriginY = (this.mouseY - this.originY) / this.scaleFactor;

			this.zoomElement.style.display = 'block';
			this.zoomElement.innerHTML = 'Zoom: ' + (parseFloat(this.scaleFactor).toFixed(1)) + 'x';

			this.updateTransform();

			return true;
		},

		reset : function()
		{
			this.zoomElement.style.display = 'none';
			this.scaleFactor = 1.0;
			this.translateX = 0.0;
			this.translateY = 0.0;
			this.translateOriginX = 0;
			this.translateOriginY = 0;
			this.mouseX = 0.0;
			this.mouseY = 0.0;
			this.originX = 0.0;
			this.originY = 0.0;
			this.startScrollPosition = null;
		},

		panTo : function(x, y)
		{
			this.reset();

			this.translateX = x;
			this.translateY = y;
			this.lastTranslateX = this.translateX;
			this.lastTranslateY = this.translateY;

			this.updateTransform();
		},

		updateTransform : function()
		{
			let scale = 'scale3d('+this.scaleFactor+','+this.scaleFactor+','+this.scaleFactor+')';
			let translateOrigin = 'translate3d(' + (this.translateOriginX) + 'px,' + (this.translateOriginY) + 'px, 0)';
			let translate = 'translate3d(' + (this.translateX) + 'px,' + (this.translateY) + 'px, 0)';

			this.containerElement.style.transformOrigin = this.originX + 'px ' + this.originY + 'px';
			this.containerElement.style.transform = scale + ' ' +translateOrigin + ' ' + translate;
		},

		onDragChanged : function(event)
		{
			if(event.target != this.containerElement)
				return;

			if(this.draggedNode == null)
				return;

			let invScaleFactor = 1.0 / this.scaleFactor;

			let currentX = parseInt(this.draggedNode.style.left);
			let currentY = parseInt(this.draggedNode.style.top);
			let currentWidth = this.draggedNode.offsetWidth;
			let currentHeight= this.draggedNode.offsetHeight;

			let movementX = event.movementX * invScaleFactor;
			let movementY = event.movementY * invScaleFactor;

			let finalX = currentX + movementX;
			let finalY = currentY + movementY;

			let containerWidth = parseInt(this.containerElement.style.width);
			let containerHeight = parseInt(this.containerElement.style.height);

			if(finalX < 0
				|| finalY < 0
				|| finalX + currentWidth >= containerWidth
				|| finalY + currentHeight >= containerHeight)
				return;

			this.draggedNode.style.left = finalX + 'px';
			this.draggedNode.style.top = finalY + 'px';

			ShaderGraphExtractor.recalculateElements(this.draggedNode,
				movementX,
				movementY);
		},

		onDragStarted : function(event, draggedNode)
		{
			if(event.target.className == 'chevron' || event.target.className == 'settings')
				return;

			if(event.buttons != 1)
				return;

			this.draggedNode = draggedNode;
			this.draggedNode.setAttribute('data-dragging', true);

			document.body.dataset['dragging'] = 1;
			return false;
		},

		onDragEnded : function(event)
		{
			if(this.draggedNode != null)
			{
				this.draggedNode.setAttribute('data-dragging', false);
			}

			this.draggedNode = null;
			document.body.dataset['dragging'] = 0;
			return false;
		}
	}

	let OverlaidForm = {

		button: undefined,
		element : undefined,
		opened : false,
		closeCallback : undefined,

		initialize : function(button, element, closeCallback)
		{
			if(element == undefined || button == undefined)
				return this;

			this.element = element;
			this.button = button;
			this.closeCallback = closeCallback;
			this.element.querySelector('h5 em').addEventListener('click', this.close.bind(this));

			return this;
		},

		setInnerHTML : function(innerHTML)
		{
			this.inspectorElement.querySelector('.body').innerHTML = innerHTML;
		},

		reset : function()
		{
			this.close();
		},

		close : function()
		{
			if(this.element == undefined)
				return;

			this.opened = false;
			this.element.style.display = 'none';

			this.closeCallback();
		},

		open : function()
		{
			if(this.element == undefined)
				return;

			this.opened = true;
			this.element.style.display = 'block';
		}
	}

	let FileLoader = {

		fileButton : null,
		fileInputElement : null,
		pannerDragger : null,
		containerElement : null,
		blackboard : null,
		inspector : null,
		isSubgraph : false,
		cryptoSuite : null,
		shaderGraphText : null,
		shaderGraphFilename : null,
		embedder : null,

		leftMostPoint : { x: 0, y: 0},

		initialize : function(
							fileButton,
							fileInputElement,
							containerElement,
							blackboard,
							pannerDragger,
							inspector,
							cryptoSuite,
							embedder)
		{
			this.cryptoSuite = cryptoSuite;
			this.blackboard = blackboard;
			this.containerElement = containerElement;
			this.pannerDragger = pannerDragger;
			this.fileButton = fileButton;
			this.fileInputElement = fileInputElement;
			this.inspector = inspector;
			this.embedder = embedder;

			document.addEventListener('keydown', (event) => {

				if(event.keyCode == 27)
				{
					this.hideLibrary();
					this.hideSubmission();
					this.hidePreview();
					this.embedder.close();
					this.inspector.hide();
				}
			});

			if(document.querySelector('button[data-openlibrary') != undefined)
			{
				document.querySelector('button[data-openlibrary').addEventListener('click', this.showLibrary.bind(this));
			}

			if(document.getElementById('library') != undefined)
			{
				document.getElementById('library').addEventListener('click', (event) => {
					let className = document.getElementById('library-container').className;

					this.hideSubmission();
					this.hidePreview();
					this.blackboard.hide();
					this.embedder.close();
					this.inspector.hide();

					if(className == 'open')
						this.hideLibrary();
					else
						this.showLibrary();
				});
			}

			if(this.embedder.element != undefined)
			{
				this.embedder.element.querySelector('button').addEventListener('click', () => {

					let embedElement = this.embedder.element.querySelector('input');

					this.copyTextFromElement(embedElement);
				});

				this.embedder.button.addEventListener('click', () => {

					document.body.dataset['presenting'] = '1';
					this.pannerDragger.isPresenting = true;

					this.hidePreview();
					this.hideLibrary();
					this.blackboard.hide();
					this.inspector.hide();

					this.embedder.element.style.display = 'block';
				});
			}

			if(document.getElementById('open-form') != undefined)
			{
				document.getElementById('open-form').addEventListener('click', (e) => {
					e.stopPropagation();
					e.preventDefault();

					this.hidePreview();
					this.hideLibrary();
					this.blackboard.hide();
					this.inspector.hide();

					document.body.dataset['presenting'] = '1';
					this.pannerDragger.isPresenting = true;

					document.getElementById('submission-form').style.display = 'block';
					return false;
				});
			}

			if(document.getElementById('close-submission') != undefined)
			{
				document.getElementById('close-submission').addEventListener('click', this.hideSubmission.bind(this));
			}

			if(document.getElementById('download') != undefined)
			{
				document.getElementById('download').addEventListener('click', () => {
					window.location = shaderGraphfilePath;
				});
			}

			if(document.getElementById('preview') != undefined)
			{
				let form = document.querySelector('#preview-form form');
				let formSubmit = document.querySelector('#preview-form form button[type="submit"]');

				document.getElementById('preview').addEventListener('click', () => {

					if(!this.cryptoSuite.isCryptoSupported())
					{
						alert('Sorry, your browser does not support the ability to encrypt the shadergraph! :(');
						return;
					}

					formSubmit.innerText = 'Generate preview link';
					formSubmit.removeAttribute('disabled');
					form.style.display = 'block';
					document.querySelector('#preview-link').style.display = 'none';

					this.hideSubmission();
					this.hideLibrary();
					this.blackboard.hide();
					this.inspector.hide();

					document.body.dataset['presenting'] = '1';
					this.pannerDragger.isPresenting = true;

					document.getElementById('preview-form').style.display = 'block';
				});

				form.addEventListener('submit', (event) => {

					event.preventDefault();

					formSubmit.innerText = 'Loading...';
					formSubmit.setAttribute('disabled', true);

					this.generatePreviewURL(
						this.shaderGraphFilename,
						this.shaderGraphText,
						(previewURL) => {

						if(previewURL == null)
						{
							formSubmit.innerText = 'Generate preview link';
							formSubmit.removeAttribute('disabled');
							return;
						}

						form.style.display = 'none';

						document.querySelector('#preview-link').style.display = 'block';
						document.querySelector('#preview-link').setAttribute('href', previewURL);
						document.querySelector('#preview-link').innerText = previewURL;
					});
				});
			}

			if(document.getElementById('close-preview') != undefined)
			{
				document.getElementById('close-preview').addEventListener('click', this.hidePreview.bind(this));
			}

			document.getElementById('reset').addEventListener('click', (e) => {
				e.preventDefault();

				this.hideLibrary();
				this.panToOrigin();
			});

			document.getElementById('fullscreen').addEventListener('click', (e) => {
				e.preventDefault();
				this.toggleFullScreen();
			});

			this.reset();
			this.blackboard.reset();
			this.pannerDragger.reset();

			if(typeof storedShaderGraphName != 'undefined'
				&& typeof storedShaderGraphBody != 'undefined')
			{
				this.loadShaderGraph(storedShaderGraphName, storedShaderGraphBody, false, false);
			}

			if(this.fileButton != undefined)
			{
				this.fileButton.addEventListener('click', () => {
					this.fileInputElement.click();
				});
			}

			if(this.fileInputElement != undefined)
			{
				if(this.fileInputElement.files.length > 0)
					this.loadFile(this.fileInputElement.files[0]);

				this.fileInputElement.addEventListener('change', (event) => {

					if(event.target.files.length > 0)
						this.loadFile(event.target.files[0]);
				}, false);
			}

			this.parsePreviewHash();

			return this;
		},

		hidePreview : function()
		{
			if(document.getElementById('preview-form') == undefined)
				return;

			document.body.dataset['presenting'] = '0';
			this.pannerDragger.isPresenting = false;

			document.getElementById('preview-form').style.display = 'none';
		},

		showLibrary : function()
		{
			if(document.getElementById('library') != undefined)
				document.getElementById('library').className = 'open';

			if(document.getElementById('library-container') != undefined)
				document.getElementById('library-container').className = 'open';
		},

		hideLibrary : function()
		{
			if(document.getElementById('library') != undefined)
				document.getElementById('library').className = '';

			if(document.getElementById('library-container') != undefined)
				document.getElementById('library-container').className ='close';
		},

		hideSubmission : function()
		{
			if(document.getElementById('submission-form') == undefined)
				return;

			document.body.dataset['presenting'] = '0';
			this.pannerDragger.isPresenting = false;

			document.getElementById('submission-form').style.display = 'none';
		},

		parsePreviewHash : function()
		{
			let hash = location.hash;

			if(hash.indexOf('#preview|') != 0)
				return;

			let hashComponents = hash.split('|');

			let previewID = hashComponents[1];
			let uuidKey = hashComponents[2];

			this.extractShaderGraphFromPreviewURL(previewID, uuidKey);
		},

		reset : function()
		{
			this.isSubgraph = false;
			this.shaderGraphText = null;
			this.shaderGraphFilename = null;

			document.getElementById('reset').setAttribute('disabled', true);
			document.getElementById('fullscreen').setAttribute('disabled', true);
		},

		toggleFullScreen : function()
		{
			if(document.fullscreenEnabled)
			{
				if (!document.fullscreenElement)
					document.body.requestFullscreen();
				else
					document.exitFullscreen();
			}
			else if(document.webkitFullscreenEnabled)
			{
				if (!document.webkitFullscreenElement)
					document.body.webkitRequestFullscreen();
				else
					document.webkitExitFullscreen();
			}
		},

		loadFile : function(file)
		{
			this.inspector.reset();
			this.pannerDragger.shaderLoaded = false;
			this.pannerDragger.reset();
			this.blackboard.reset();

			console.log('Loading ' + file.name + '...');

			var reader = new FileReader();
			reader.onerror = (event) => {
				console.log('Error loading! Try again.', event);
			}
			reader.onload = (event) => {

				console.log(file.name + ' loaded! Parsing...');

				this.loadShaderGraph(file.name, event.target.result, true, true);
			}

			reader.readAsText(file);
		},

		removeFake : function() {

			if (this.fakeHandler) {

				document.body.removeEventListener('click', this.fakeHandlerCallback);
				this.fakeHandler = null;
				this.fakeHandlerCallback = null;
			}

			if (this.fakeElem) {

				document.body.removeChild(this.fakeElem);
				this.fakeElem = null;
			}
		},

		copyTextFromElement : function(element)
		{
			element.select();
			element.setSelectionRange(0, element.value.length);

			var succeeded = false;

			try {
				succeeded = document.execCommand('copy');
			}
			catch (err) {
				succeeded = false;
			}

			return succeeded;
		},

		copyTextToClipboard : function(text)
		{
			this.removeFake();

			var self = this;

			this.fakeHandlerCallback = function() { self.removeFake(); };

			this.fakeHandler = document.body.addEventListener('click', this.fakeHandlerCallback) || true;

			this.fakeElem = document.createElement('textarea');
			this.fakeElem.style.fontSize = '12pt';
			this.fakeElem.style.border = '0';
			this.fakeElem.style.padding = '0';
			this.fakeElem.style.margin = '0';
			this.fakeElem.style.position = 'absolute';
			this.fakeElem.style[ document.documentElement.getAttribute('dir') == 'rtl' ? 'right' : 'left' ] = '-9999px';
			this.fakeElem.style.top = (window.pageYOffset || document.documentElement.scrollTop) + 'px';
			this.fakeElem.setAttribute('readonly', '');
			this.fakeElem.value = text;

			document.body.appendChild(this.fakeElem);

			return this.copyTextFromElement(this.fakeElem);
		},

		loadShaderGraph : function(fileName, shaderGraphText, showPreview, showLibrary)
		{
			try {
				serializedGraph = JSON.parse(shaderGraphText);
			}
			catch(e) {
				alert('Error while loading your shadergraph!');
				return;
			}
			this.shaderGraphFilename = fileName;
			this.shaderGraphText = shaderGraphText;

			this.pannerDragger.shaderLoaded = true;

			if(document.getElementById('readme') != undefined)
				document.getElementById('readme').style.display = 'none';

			if(document.getElementById('download') != undefined)
				document.getElementById('download').style.display = 'flex';

			this.blackboard.blackboardButton.removeAttribute('disabled');
			document.getElementById('reset').removeAttribute('disabled');

			if(document.fullscreenEnabled === true || document.webkitFullscreenEnabled === true)
				document.getElementById('fullscreen').removeAttribute('disabled');

			let name = fileName.split('.')[0];

			this.isSubgraph = fileName.split('.')[1] == 'shadersubgraph';

			if(showLibrary && document.getElementById('open-form') != undefined)
			{
				[...document.querySelectorAll('#submission-form form input')].forEach((element) => {

					if(element.name != 'full-name' && element.name != 'form-name')
						element.value = '';
				});

				document.getElementById('open-form').style.display = 'flex';

				document.getElementById('form-subgraph').style.display = (this.isSubgraph ? 'flex' : 'none');
				document.getElementById('shadergraph-sub').value = (this.isSubgraph ? 1 : 0);
				document.getElementById('shadergraph-body').value = shaderGraphText;
				document.getElementById('form-filename').value = name;
			}

			if(showPreview && document.getElementById('preview') != undefined)
			{
				document.getElementById('preview').style.display = 'flex';
			}

			this.leftMostPoint = ShaderGraphExtractor.initialize(
				name,
				serializedGraph,
				this.containerElement,
				this.blackboard,
				this.inspector,
				(event, draggedElement) => {
					return this.pannerDragger.onDragStarted(event, draggedElement);
				});

			if(this.leftMostPoint == null)
			{
				alert('No nodes found!');
				return;
			}

			this.panToOrigin();
		},

		panToOrigin : function()
		{
			this.pannerDragger.panTo(window.innerWidth / 2 - this.leftMostPoint.x,
									 window.innerHeight / 2 - this.leftMostPoint.y);
		},

		extractShaderGraphFromPreviewURL(previewID, uuidKey)
		{
			let request = new XMLHttpRequest();
			let url = '/.netlify/functions/shadergraph-action';

			let payload = {
				'actions' : [
				{
					'name': 'extractPreview',
					'value': previewID
				}
				]
			};

			request.onreadystatechange = () => {

				if(request.readyState == 4 && request.status == 200)
				{
					let encrypted = request.responseText;

					this.cryptoSuite.decrypt(encrypted, uuidKey)
						.then((decrypted) => {

							let shaderGraphStruct = JSON.parse(decrypted);

							this.loadShaderGraph(shaderGraphStruct.name, shaderGraphStruct.body, false, false);
					});
				}
			}
			request.overrideMimeType("application/json");
			request.open("POST", url);
			request.send('payload=' + JSON.stringify(payload));
		},

		generatePreviewURL(fileName, shaderGraphText, callback)
		{
			let uuidKey = this.cryptoSuite.generateUUIDV4();

			let shaderGraphStruct = {
				'name' : fileName,
				'body' : shaderGraphText
			};

			let message = JSON.stringify(shaderGraphStruct);

			this.cryptoSuite.encrypt(message, uuidKey)
				.then((encrypted) => {

					let params = [
					{
						'name': 'form-name',
						'value': 'previews'
					},
					{
						'name': 'name',
						'value': 'Preview'
					},
					{
						'name': 'shadergraph',
						'value': encrypted
					}
					].map((el) => {
						return encodeURIComponent(el.name) + '=' + encodeURIComponent(el.value);
					}).join('&');

					let req = new XMLHttpRequest();
					req.onreadystatechange = function() {

						if(req.readyState != 4)
							return;

						if(req.status != 200)
						{
							alert('Error, try again.');
							callback(null);
							return;
						}

						let responseHTML = req.responseText;
						let parser = new DOMParser();
						let doc = parser.parseFromString(responseHTML, 'text/html');
						let script = doc.querySelector('script');

						if(script == undefined)
						{
							alert('Error, try again.');
							callback(null);
							return;
						}

						let scriptContents = script.innerText;

						let trimStart = scriptContents.indexOf('= {') + 2
						let trimEnd = scriptContents.length - 1
						let jsonContents = scriptContents.substring(trimStart, trimEnd);

						let responseJSON = JSON.parse(jsonContents);

						let previewID = responseJSON.id;

						let previewURL = location.protocol + '//' + location.host + '/#preview|' + previewID + '|' + uuidKey;

						callback(previewURL);
					}
					req.open("POST", "/");
					req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
					req.send(params);
			});
		}
	}

	let ShaderGraphExtractor = {

		propertyNodeType : 'UnityEditor.ShaderGraph.PropertyNode',
		customFunctionNodeType : 'UnityEditor.ShaderGraph.CustomFunctionNode',
		vector3NodeType : 'UnityEditor.ShaderGraph.Vector3Node',
		randomRangeNodeType : 'UnityEditor.ShaderGraph.RandomRangeNode',
		gradientNodeType: 'UnityEditor.ShaderGraph.SampleGradient',
		blendNodeType: 'UnityEditor.ShaderGraph.BlendNode',
		addNodeType : 'UnityEditor.ShaderGraph.AddNode',
		gradientNoiseNodeType : 'UnityEditor.ShaderGraph.GradientNoiseNode',
		remapNodeType : 'UnityEditor.ShaderGraph.RemapNode',
		subtractNodeType : 'UnityEditor.ShaderGraph.SubtractNode',
		multiplyNodeType : 'UnityEditor.ShaderGraph.MultiplyNode',
		divideNodeType : 'UnityEditor.ShaderGraph.DivideNode',
		unlitMasterNodeType: 'UnityEditor.ShaderGraph.UnlitMasterNode',
		subgraphNodeType: 'UnityEditor.ShaderGraph.SubGraphNode',

		vector1ShaderProperty: 'UnityEditor.ShaderGraph.Vector1ShaderProperty',
		vector1ShaderPropertyInternal: 'UnityEditor.ShaderGraph.Internal.Vector1ShaderProperty',
		vector2ShaderProperty: 'UnityEditor.ShaderGraph.Vector2ShaderProperty',
		vector2ShaderPropertyInternal: 'UnityEditor.ShaderGraph.Internal.Vector2ShaderProperty',
		vector3ShaderProperty: 'UnityEditor.ShaderGraph.Vector3ShaderProperty',
		vector3ShaderPropertyInternal: 'UnityEditor.ShaderGraph.Internal.Vector3ShaderProperty',
		vector4ShaderProperty: 'UnityEditor.ShaderGraph.Vector4ShaderProperty',
		vector4ShaderPropertyInternal: 'UnityEditor.ShaderGraph.Internal.Vector4ShaderProperty',
		textureShaderProperty: 'UnityEditor.ShaderGraph.TextureShaderProperty',
		textureShaderPropertyInternal: 'UnityEditor.ShaderGraph.Internal.Texture2DShaderProperty',
		colorShaderProperty: 'UnityEditor.ShaderGraph.ColorShaderProperty',
		colorShaderPropertyInternal: 'UnityEditor.ShaderGraph.Internal.ColorShaderProperty',
		booleanShaderProperty: 'UnityEditor.ShaderGraph.BooleanShaderProperty',
		booleanShaderPropertyInternal: 'UnityEditor.ShaderGraph.Internal.BooleanShaderProperty',

		texture2DInputSlot: 'UnityEditor.ShaderGraph.Texture2DInputMaterialSlot',
		samplerSlot: 'UnityEditor.ShaderGraph.SamplerStateMaterialSlot',
		texture2DSlot: 'UnityEditor.ShaderGraph.Texture2DMaterialSlot',
		gradientSlot: 'UnityEditor.ShaderGraph.GradientMaterialSlot',
		gradientInputSlot: 'UnityEditor.ShaderGraph.GradientInputMaterialSlot',
		booleanSlot: 'UnityEditor.ShaderGraph.BooleanMaterialSlot',

		debugEnableSlotDimensions: true,
		debugUseBezier: false,

		mode : {
			0: "Default",
			1: "Slider",
			2: "Integer",
			3: "Enum"
		},

		precision : {
			0 : "Inherit",
			1 : "Float",
			2 : "Half"
		},

		colorMode : {
			0: "Default",
			1: "HDR"
		},

		edgeTable : {},
		edgeElements : {},
		slotInputs : {},
		slotOutputs : {},
		serializedProperties : {},
		groupTable : {},

		canvasSize : {
			width : 0,
			height: 0
		},
		extraHorizontalPadding: 500,
		extraVerticalPadding: 500,

		svgElement : null,
		containerElement: null,
		dragStartCallback: null,
		filename: null,
		blackboard: null,
		inspector: null,

		isEmbedded : false,

		initialize : function(filename, serializedGraph, containerElement, blackboard, inspector, dragStartCallback)
		{
			this.isEmbedded = (document.body.className == 'page-embed');
			this.inspector = inspector;
			this.filename = filename;
			this.blackboard = blackboard;
			this.dragStartCallback = dragStartCallback;
			this.containerElement = containerElement;

			return this.parse(serializedGraph);
		},

		getPosition : function(el)
		{
			var xPos = 0;
			var yPos = 0;

			while (el != document.body) {

				if (el.tagName == "BODY") {
					// deal with browser quirks with body/window/document and page scroll
					var xScroll = el.scrollLeft || document.documentElement.scrollLeft;
					var yScroll = el.scrollTop || document.documentElement.scrollTop;

					xPos += (el.offsetLeft - xScroll + el.clientLeft);
					yPos += (el.offsetTop - yScroll + el.clientTop);
				} else {
					// for all other non-BODY elements
					xPos += (el.offsetLeft - el.scrollLeft + el.clientLeft);
					yPos += (el.offsetTop - el.scrollTop + el.clientTop);
				}

				el = el.offsetParent;
			}

			return {
				x: xPos,
				y: yPos
			};
		},

		moveGroupWithGuid : function(groupGuid, movementX, movementY)
		{
			this.groupTable[groupGuid].forEach((nodeGUID) => {
				let nodeElement = document.getElementById(nodeGUID);
				let currentX = parseInt(nodeElement.style.left);
				let currentY = parseInt(nodeElement.style.top);

				nodeElement.style.left = (currentX + movementX) + 'px';
				nodeElement.style.top = (currentY + movementY) + 'px';

				if(this.edgeElements[nodeGUID] != undefined)
				{
					this.edgeElements[nodeGUID].forEach((serializedEdge) => {
						this.updateOrCreateEdge(serializedEdge);
					});
				}
			});
		},

		moveStickyWithGuid : function(guid)
		{
			let groupGuid = document.getElementById(guid).dataset['groupguid'];

			if(groupGuid == undefined)
				return;

			let group = document.getElementById(groupGuid);

			if(group == undefined)
				return;

			this.resizeGroup(group);
		},

		moveNodeWithGuid : function(guid)
		{
			this.edgeElements[guid].forEach((serializedEdge) => {
				this.updateOrCreateEdge(serializedEdge);
			});

			let groupGuid = document.getElementById(guid).dataset['groupguid'];

			if(groupGuid == undefined)
				return;

			let group = document.getElementById(groupGuid);

			if(group == undefined)
				return;

			this.resizeGroup(group);
		},

		recalculateElements : function(draggedNode, movementX, movementY)
		{
			let guid = draggedNode.id;

			if(draggedNode.className == 'group')
				this.moveGroupWithGuid(guid, movementX, movementY);
			else if(draggedNode.className == 'sticky')
				this.moveStickyWithGuid(guid);
			else
				this.moveNodeWithGuid(guid);
		},

		disableChevronIfNeededForNode : function(nodeElement)
		{
			if([...nodeElement.querySelectorAll('.slot-container li.filled')].length != [...nodeElement.querySelectorAll('.slot-container li')].length)
				return;

			let chevron = nodeElement.querySelector('.chevron');

			if(chevron == undefined)
				return;

			chevron.dataset['disabled'] = '1';
		},

		updateOrCreateEdge : function(serializedEdge)
		{
			let inputSlot = serializedEdge['m_InputSlot'];
			let inputGuid = inputSlot['m_NodeGUIDSerialized'];
			let inputSlotId = inputSlot['m_SlotId'];
			let inputElement = this.edgeTable[inputGuid]['i'][inputSlotId];

			let outputSlot = serializedEdge['m_OutputSlot'];
			let outputGuid = outputSlot['m_NodeGUIDSerialized'];
			let outputSlotId = outputSlot['m_SlotId'];
			let outputElement = this.edgeTable[outputGuid]['o'][outputSlotId];

			if(inputElement == null
				|| outputElement == null)
				return;

			inputElement.className = 'filled';
			inputElement.parentNode.className = 'filled';
			outputElement.className = 'filled';
			outputElement.parentNode.className = 'filled';

			let inputNodeElement = document.getElementById(inputGuid);
			let outputNodeElement = document.getElementById(outputGuid);

			this.disableChevronIfNeededForNode(inputNodeElement);
			this.disableChevronIfNeededForNode(outputNodeElement);

			var positionIn = this.getPosition(inputElement);
			positionIn.x -= 18;
			positionIn.y -= 18;

			var positionOut = this.getPosition(outputElement);
			positionOut.x -= 18;
			positionOut.y -= 18;

			let width = Math.abs(positionIn.x - positionOut.x);
			let height= Math.abs(positionIn.y - positionOut.y);

			let threshold = 50;

			let isBXFirst = positionOut.x < positionIn.x;

			let svgX1 = (isBXFirst ? positionOut.x : positionIn.x);
			let svgY1 = (isBXFirst ? positionOut.y : positionIn.y);
			let svgX2 = (!isBXFirst ? positionOut.x : positionIn.x);
			let svgY2 = (!isBXFirst ? positionOut.y : positionIn.y);

			let centerY = (positionIn.y < positionOut.y ? positionIn.y + (positionOut.y - positionIn.y)/2 : positionOut.y + (positionIn.y - positionOut.y)/2);

			let edgeId = inputGuid + '|' + outputGuid + '|' + inputSlotId + '|' + outputSlotId;
			var groupElement = this.svgElement.getElementById(edgeId);

			if(groupElement == undefined)
			{
				groupElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
				groupElement.setAttribute('id', edgeId);
				this.svgElement.appendChild(groupElement);
			}

			let strokeWidth = 4;
			let lineWidth = (isBXFirst ? 1 : -1) * 30.0;

			let vectorColors = {
				'-1': 'rgb(193,193,193)',
				'1': 'rgb(122,223,226)',
				'2': 'rgb(144,236,138)',
				'3': 'rgb(244,255,149)',
				'4': 'rgb(250,197,241)'
			};

			let otherColor = 'rgb(244,125,125)';

			var inputColor = undefined;
			var outputColor = undefined;

			if(this.debugEnableSlotDimensions)
			{
				inputColor = otherColor;
				outputColor = otherColor;

				if(inputElement.parentNode.dataset['value'] != undefined
					&& outputElement.parentNode.dataset['value'] != undefined)
				{
					let inputValue = inputElement.parentNode.dataset['value'];
					let outputValue = outputElement.parentNode.dataset['value'];

					if(vectorColors[inputValue] != undefined)
						inputColor = vectorColors[inputValue];
					else
						console.log(inputValue, inputElement);

					if(vectorColors[outputValue] != undefined)
						outputColor = vectorColors[outputValue];
					else
						console.log(outputValue, outputElement);
				}
			}
			else
			{
				inputColor = vectorColors[1]
				outputColor = vectorColors[1];
			}

			let gradient = this.svgGradient(outputColor, inputColor);

			if(this.svgElement.querySelector('defs #' + gradient.id) == undefined)
				this.svgElement.querySelector('defs').appendChild(gradient);

			if(svgY2 - svgY1 == 0)
				svgY2 += 1;

			if(height == 0 || width == Math.abs(lineWidth) * 2.0)
				groupElement.innerHTML = '<line stroke-linejoin="round" stroke-linecap="round" x1="' + svgX1 + '" y1="' + svgY1 + '" x2="' + svgX2 + '" y2="' + svgY2 + '" stroke="url(#' + gradient.id + ')" stroke-width="' + strokeWidth + '"/>';
			else
			{
				groupElement.innerHTML = '<line stroke-linejoin="round" stroke-linecap="round" x1="' + svgX1 + '" y1="' + svgY1 + '" x2="' + (svgX1 + lineWidth) + '" y2="' + svgY1 + '" stroke="' + outputColor + '" stroke-width="' + strokeWidth + '" />';

				if(this.debugUseBezier)
					groupElement.innerHTML += '<path stroke-linejoin="round" stroke-linecap="round" d="M' + (svgX1 + lineWidth) + ',' + svgY1 + ' Q' + (svgX1 + lineWidth + width / 2) + ',' + (centerY) + ' ' + (svgX2 - lineWidth) + ',' + svgY2 + '" version="1.1" xmlns="http://www.w3.org/1999/xhtml" fill="none" stroke="url(#' + gradient.id + ')" stroke-width="' + strokeWidth + '"></path>';
				else
					groupElement.innerHTML += '<line stroke-linejoin="round" stroke-linecap="round" x1="' + (svgX1 + lineWidth) + '" y1="' + svgY1 + '" x2="' + (svgX2 - lineWidth) + '" y2="' + svgY2 + '" stroke="url(#' + gradient.id + ')" stroke-width="' + strokeWidth + '"/>';

				groupElement.innerHTML += '<line stroke-linejoin="round" stroke-linecap="round" x1="' + (svgX2 - lineWidth) + '" y1="' + svgY2 + '" x2="' + svgX2 + '" y2="' + svgY2 + '" stroke="' + inputColor + '" stroke-width="' + strokeWidth + '"/>';
			}
		},

		svgGradient : function(fromColor, toColor)
		{
			let element = document.createElementNS("http://www.w3.org/2000/svg", "linearGradient");
			element.id = 'gradient-' + fromColor.replace(/\(|\)|\,/g, "") + '-' + toColor.replace(/\(|\)|\,/g, "");
			element.setAttribute('x1', '0%');
			element.setAttribute('y1', '0%');
			element.setAttribute('x2', '100%');
			element.setAttribute('y2', '0%');
			element.innerHTML = '<stop offset="0%" style="stop-color:' + fromColor + ';" /><stop offset="100%" style="stop-color:' + toColor + ';" />';

			return element;
		},

		parse : function(serializedGraph)
		{
			this.containerElement.innerHTML = "";
			this.containerElement.style.transform = "";

			this.edgeElements = {};
			this.edgeTable = {};
			this.slotInputs = {}
			this.slotOutputs = {};
			this.serializedProperties = {};
			this.canvasSize = {
				width : 0,
				height: 0
			};
			this.groupTable = {};
			this.svgElement = null;

			let nodes = serializedGraph['m_SerializableNodes'];
			let properties = serializedGraph['m_SerializedProperties'];
			let stickyNotes = serializedGraph['m_StickyNotes'];
			let groups = serializedGraph['m_Groups'];

			if(nodes == undefined)
				return null;

			var blackboardHeader = this.filename.split(/\./)[0];

			if(serializedGraph['m_Path'] != undefined)
				blackboardHeader += '<span>' + serializedGraph['m_Path'] + '</span>';

			this.blackboard.blackboardElement.querySelector('h5').innerHTML = blackboardHeader;

			console.log('Parsing properties...');

			if(properties != undefined
				&& properties.length > 0)
			{
				properties.forEach((property) => {
					let propertyData = property['JSONnodeData'];
					let serializedProperty = JSON.parse(propertyData);

					let listElement = document.createElement('li');

					let isExposed = serializedProperty['m_GeneratePropertyBlock'] === true;

					var innerHTML = '<header>';

					innerHTML += '<div class="left">';
					innerHTML += '<em data-expanded="1" class="chevron"></em>'
					innerHTML += '<h3';

					if(isExposed)
						innerHTML += ' class="exposed"';

					innerHTML += '>' + serializedProperty['m_Name'];
					innerHTML += '</h3>';
					innerHTML += '</div>';

					let typeInfo = property['typeInfo']['fullName'];

					if(typeInfo == this.vector1ShaderProperty || typeInfo == this.vector1ShaderPropertyInternal)
						innerHTML += '<span>Vector1</span>';
					else if(typeInfo == this.vector2ShaderProperty || typeInfo == this.vector2ShaderPropertyInternal)
						innerHTML += '<span>Vector2</span>';
					else if(typeInfo == this.vector3ShaderProperty || typeInfo == this.vector3ShaderPropertyInternal)
						innerHTML += '<span>Vector3</span>';
					else if(typeInfo == this.vector4ShaderProperty || typeInfo == this.vector4ShaderPropertyInternal)
						innerHTML += '<span>Vector4</span>';
					else if(typeInfo == this.textureShaderProperty || typeInfo == this.textureShaderPropertyInternal)
						innerHTML += '<span>Texture</span>';
					else if(typeInfo == this.colorShaderProperty || typeInfo == this.colorShaderPropertyInternal)
						innerHTML += '<span>Color</span>';
					else if(typeInfo == this.booleanShaderProperty || typeInfo == this.booleanShaderPropertyInternal)
						innerHTML += '<span>Boolean</span>';

					innerHTML += '</header>';

					let reference = (serializedProperty['m_OverrideReferenceName'] != undefined && serializedProperty['m_OverrideReferenceName'] !='' ? serializedProperty['m_OverrideReferenceName'] : serializedProperty['m_DefaultReferenceName']);

					var value = serializedProperty['m_Value'];

					if(typeof value == 'object')
					{
						if(typeInfo == this.textureShaderProperty || typeInfo == this.textureShaderPropertyInternal)
							value = '<em class="texture"></em>Texture';
						else if(typeInfo == this.colorShaderProperty || typeInfo == this.colorShaderPropertyInternal)
						{
							let colorValue = value;

							value = '<div class="color-container">';
							value += '<div class="color" style="background-color:rgb('+(255*colorValue['r'])+','+(255*colorValue['g'])+','+(255*colorValue['b'])+');">&nbsp;</div>';
							value += '<div class="opacity" style="width:' + (parseFloat(colorValue['a']) * 100) + '%">&nbsp;</div>';
							value += '</div>';
						}
						else if(typeInfo == this.vector2ShaderProperty || typeInfo == this.vector2ShaderPropertyInternal)
							value = 'X:' + this.normalizedSlotValue(value['x']) + ' Y:' + this.normalizedSlotValue(value['y']);
						else if(typeInfo == this.vector3ShaderProperty || typeInfo == this.vector3ShaderPropertyInternal)
							value = 'X:' + this.normalizedSlotValue(value['x']) + ' Y:' + this.normalizedSlotValue(value['y']) + ' Z:' + this.normalizedSlotValue(value['z']);
						else if(typeInfo == this.vector4ShaderProperty || typeInfo == this.vector4ShaderPropertyInternal)
							value = 'X:' + this.normalizedSlotValue(value['x']) + ' Y:' + this.normalizedSlotValue(value['y']) + ' Z:' + this.normalizedSlotValue(value['z']) + ' W:' + this.normalizedSlotValue(value['w']);
						else
							console.log(typeInfo, serializedProperty);
					}
					else if(typeof value == 'boolean')
					{
						value = (value === true ? 'Yes' : 'No');
					}
					else if(typeof value == 'number')
					{
						value = this.normalizedSlotValue(value);
					}

					innerHTML += '<dl>';
					innerHTML += '<dt>Exposed</dt><dd>' + (isExposed ? 'Yes' : 'No') + '</dd>';
					innerHTML += '</dl>';

					innerHTML += '<dl>';
					innerHTML += '<dt>Reference</dt><dd>' + reference + '</dd>';
					innerHTML += '</dl>';

					innerHTML += '<dl>';
					innerHTML += '<dt>Default</dt><dd>' + value + '</dd>';
					innerHTML += '</dl>';

					if(serializedProperty['m_FloatType'] != undefined)
					{
						innerHTML += '<dl>';
						innerHTML += '<dt>Mode</dt><dd>' + this.mode[serializedProperty['m_FloatType']] + '</dd>';
						innerHTML += '</dl>';
					}

					if(serializedProperty['m_ColorMode'] != undefined)
					{
						innerHTML += '<dl>';
						innerHTML += '<dt>Mode</dt><dd>' + this.colorMode[serializedProperty['m_ColorMode']] + '</dd>';
						innerHTML += '</dl>';
					}

					innerHTML += '<dl>';
					innerHTML += '<dt>Precision</dt><dd>';

					if(serializedProperty['m_Precision'] == undefined)
						innerHTML += this.precision[0] + '</dd>';
					else
						innerHTML += this.precision[serializedProperty['m_Precision']] + '</dd>';

					innerHTML += '</dl>';

					listElement.innerHTML = innerHTML;

					listElement.querySelector('.chevron').addEventListener('click', function() {

						let expanded = (this.dataset['expanded'] == "1");

						this.dataset['expanded'] = (expanded ? "0" : "1");

						[...listElement.querySelectorAll('dl')].forEach((el) => {
							el.style.display = expanded ? 'none' : 'flex';
						})
					});

					this.blackboard.blackboardElement.querySelector('ul').appendChild(listElement);
					this.serializedProperties[serializedProperty['m_Guid']['m_GuidSerialized']] = serializedProperty;
				});

				if(!this.isEmbedded)
					this.blackboard.show();
			}

			console.log('Calculating canvas size...');

			var minPoint = undefined;
			var maxPoint = undefined;
			var leftMostStruct = undefined;

			nodes.forEach((node) => {

				let nodeData = node['JSONnodeData'];
				let serializedNode = JSON.parse(nodeData);
				let drawState = serializedNode['m_DrawState'];
				let position = drawState['m_Position'];

				let nodePositionX = position.x;
				let nodePositionY = position.y;

				if(minPoint == undefined)
				{
					minPoint = {
						x: nodePositionX,
						y: nodePositionY
					}
				}

				if(maxPoint == undefined)
				{
					maxPoint = {
						x: nodePositionX,
						y: nodePositionY
					}
				}

				if(nodePositionX < minPoint.x)
					minPoint.x = nodePositionX;

				if(nodePositionY < minPoint.y)
					minPoint.y = nodePositionY;

				if(nodePositionX > maxPoint.x)
					maxPoint.x = nodePositionX;

				if(nodePositionY > maxPoint.y)
					maxPoint.y = nodePositionY;

				let guid = serializedNode['m_GuidSerialized'];

				let element = {
					x: nodePositionX,
					y: nodePositionY,
					'guid': guid
				};

				if(leftMostStruct == undefined
					|| nodePositionX < leftMostStruct.x)
					leftMostStruct = element;
			});

			if(stickyNotes != undefined
				&& stickyNotes.length > 0)
			{
				stickyNotes.forEach((stickyNote) => {

					let stickyPosition = stickyNote['m_Position'];

					let nodePositionX = stickyPosition.x;
					let nodePositionY = stickyPosition.y;

					if(nodePositionX < minPoint.x)
						minPoint.x = nodePositionX;

					if(nodePositionY < minPoint.y)
						minPoint.y = nodePositionY;

					if(nodePositionX > maxPoint.x)
						maxPoint.x = nodePositionX;

					if(nodePositionY > maxPoint.y)
						maxPoint.y = nodePositionY;

					let guid = stickyNote['m_GuidSerialized'];

					let element = {
						x: nodePositionX,
						y: nodePositionY,
						'guid': guid
					};

					if(leftMostStruct == undefined
						|| nodePositionX < leftMostStruct.x)
						leftMostStruct = element;
				});
			}

			let width = parseInt(Math.abs(minPoint.x - maxPoint.x));
			let height = parseInt(Math.abs(minPoint.y - maxPoint.y));
			let finalWidth = width + (2 * this.extraHorizontalPadding);
			let finalHeight = height + (2 * this.extraVerticalPadding);
			let startX = minPoint.x - this.extraHorizontalPadding;
			let startY = minPoint.y - this.extraVerticalPadding;

			this.canvasSize.width = finalWidth;
			this.canvasSize.height= finalHeight;

			this.containerElement.style.width = this.canvasSize.width + 'px';
			this.containerElement.style.height = this.canvasSize.height + 'px';

			console.log('Drawing nodes...');

			nodes.forEach((node) => {

				let nodeData = node['JSONnodeData'];
				let serializedNode = JSON.parse(nodeData);
				let drawState = serializedNode['m_DrawState'];
				let position = drawState['m_Position'];
				let typeInfo = node['typeInfo'];
				let fullNodeName = typeInfo.fullName;
				let name = serializedNode['m_Name'];
				let guid = serializedNode['m_GuidSerialized'];
				let nodePositionX = position.x - startX;
				let nodePositionY = position.y - startY;
				let expanded = drawState['m_Expanded'];
				let groupGuid = serializedNode['m_GroupGuidSerialized'];

				if(this.groupTable[groupGuid] == undefined)
					this.groupTable[groupGuid] = [];

				this.groupTable[groupGuid].push(guid);

				let nodeElement = document.createElement('div');
				nodeElement.className = "node";

				if(fullNodeName == this.propertyNodeType)
					nodeElement.className += ' property';

				nodeElement.dataset['expanded'] = (expanded === true ? '1' : '0');
				nodeElement.dataset['fullname'] = fullNodeName;
				nodeElement.dataset['groupguid'] = groupGuid;
				nodeElement.setAttribute('title', name);
				nodeElement.setAttribute('id', guid);
				nodeElement.style.top = nodePositionY + 'px';
				nodeElement.style.left = nodePositionX + 'px';
				nodeElement.style.minWidth = position.width + 'px';

				let self = this;

				nodeElement.onmousedown = function(event)
				{
					if(event.detail == 2
						&& serializedNode['m_SerializedSubGraph'] != undefined)
					{
						let serializedSubGraph = JSON.parse(serializedNode['m_SerializedSubGraph']);
						let subgraphGUID = serializedSubGraph['subGraph']['guid'];

						if(typeof subgraphs != 'undefined'
							&& subgraphs[subgraphGUID] != undefined)
						{
							let subgraphID = subgraphs[subgraphGUID];
							let url = '/library/' + subgraphID + '/';

							if(this.isEmbedded)
								window.open(url, '_blank');
							else
								window.location = url;

							return;
						}
					}

					if(!self.isEmbedded)
						self.dragStartCallback(event, this);
				}

				if(!self.isEmbedded)
				{
					nodeElement.onmouseenter = function(event) {
						this.dataset['hovering'] = true;
						document.body.dataset['hovering'] = 1;
					}
					nodeElement.onmouseleave = function(event) {
						this.dataset['hovering'] = false;
						document.body.dataset['hovering'] = 0;
					}
				}

				let propertyGuid = serializedNode['m_PropertyGuidSerialized'];

				if(fullNodeName != this.propertyNodeType)
				{
					let nodeHeader = document.createElement('h5');

					let innerHTML = '<span>' + name + '</span>';

					innerHTML += '<span class="tools">';

					var showSettings = false;

					if(fullNodeName == this.customFunctionNodeType
						|| fullNodeName == this.subgraphNodeType)
					{
						showSettings = true;
						innerHTML += '<em class="settings"></em>';
					}

					innerHTML += '<em data-expanded="' + (expanded === true ? '1' : '0') + '" class="chevron"></em>';

					innerHTML += '</span>';

					nodeHeader.innerHTML = innerHTML;

					nodeElement.appendChild(nodeHeader);
					let self = this;
					nodeElement.querySelector('.chevron').addEventListener('click', function(event) {

						if(this.dataset['disabled'] == '1')
							return;

						let expanded = (nodeElement.dataset['expanded'] == '1');

						if(expanded)
						{
							[...nodeElement.querySelectorAll('.slot-container li:not(.filled)')].forEach((el) => el.style.display = 'none');
							this.dataset['expanded'] = '0';
							nodeElement.dataset['expanded'] = '0';
						}
						else
						{
							[...nodeElement.querySelectorAll('.slot-container li:not(.filled)')].forEach((el) => el.style.display = 'flex');
							this.dataset['expanded'] = '1';
							nodeElement.dataset['expanded'] = '1';
						}

						self.moveNodeWithGuid(nodeElement.id);

						event.preventDefault();
					});

					if(showSettings)
					{
						nodeElement.querySelector('.settings').addEventListener('click', (event) => {

							this.inspector.setHeader(name);

							let innerHTML = '<dl>';

							if(fullNodeName == this.customFunctionNodeType)
							{
								innerHTML += '<dt>Type</dt><dd>' + serializedNode['m_SourceType'] + '</dd>';
								innerHTML += '</dl>';
								innerHTML += '<dl>';
								innerHTML += '<dt>Name</dt><dd>' + serializedNode['m_FunctionName'] + '</dd>';
								innerHTML += '</dl>';
								innerHTML += '<dl>';
								innerHTML += '<dt>Source</dt>';
								innerHTML += '</dl>';
								innerHTML += '<textarea>';
								innerHTML += serializedNode['m_FunctionBody'];
								innerHTML += '</textarea>';
							}
							else if(fullNodeName == this.subgraphNodeType)
							{
								let serializedSubGraph = JSON.parse(serializedNode['m_SerializedSubGraph']);
								let subgraphGUID = serializedSubGraph['subGraph']['guid'];

								if(typeof subgraphs != 'undefined'
									&& subgraphs[subgraphGUID] != undefined)
								{
									let subgraphID = subgraphs[subgraphGUID];

									innerHTML += '<dt>SubGraph GUID: </dt><dd><a href="/library/' + subgraphID + '/">' + serializedSubGraph['subGraph']['guid'] + '</a></dd>';
								}
								else
									innerHTML += '<dt>SubGraph GUID: </dt><dd>' + serializedSubGraph['subGraph']['guid'] + '</dd>';
							}

							innerHTML += '</dl>';

							this.inspector.setInnerHTML(innerHTML);

							this.inspector.show();
						});
					}
				}
				else if(this.serializedProperties[propertyGuid] != undefined)
				{
					let serializedProperty = this.serializedProperties[propertyGuid];

					nodeElement.setAttribute('data-generatepropertyblock', serializedProperty['m_GeneratePropertyBlock']);
				}

				let slotContainer = document.createElement('div');
				slotContainer.className = 'slot-container';

				nodeElement.appendChild(slotContainer);

				var inputs = [];
				var outputs = [];

				let slots = serializedNode['m_SerializableSlots'];

				this.edgeTable[guid] = {
					'i' : {},
					'o' : {}
				};

				slots.forEach((slot) => {

					let slotData = slot['JSONnodeData'];
					let typeInfo = slot['typeInfo'];
					let fullSlotName = typeInfo.fullName;
					let serializedSlot = JSON.parse(slotData);
					let type = serializedSlot['m_SlotType'];
					let id = serializedSlot['m_Id'];
					let defaultValue = serializedSlot['m_DefaultValue'];
					var name = serializedSlot['m_DisplayName'];

					if(this.debugEnableSlotDimensions)
					{
						var value = undefined;

						if(fullSlotName == this.gradientSlot
							|| fullSlotName == this.gradientInputSlot)
						{
							value = -1;
							name += ' (<i>G</i>)';
						}
						else if(fullSlotName == this.booleanSlot)
						{
							value = 1;
							name += ' (<i>1</i>)';
						}
						else if(typeof defaultValue == 'object')
						{
							value = Object.keys(defaultValue).length;
							name += ' (<i>' + value + '</i>)';
						}
						else if(typeof defaultValue == 'number')
						{
							value = 1;
							name += ' (<i>' + value + '</i>)';
						}
						else if(fullSlotName == this.texture2DSlot
							|| fullSlotName == this.texture2DInputSlot)
						{
							name += ' (<i>T2</i>)';
						}
						else if(fullSlotName == this.samplerSlot)
						{
							name += ' (<i>SS</i>)';
						}
						else
							console.log(serializedSlot, typeInfo);
					}

					if(type == 0)
						inputs.push([name, id, value, serializedSlot]);
					else if(type == 1)
						outputs.push([name, id, value, serializedSlot]);
				});

				if(inputs.length > 0)
				{
					let slotInputs = document.createElement('ul');
					slotInputs.className = 'slot-inputs';
					slotContainer.appendChild(slotInputs);

					inputs.forEach((input) => {

						let slotElement = document.createElement('li');
						slotElement.setAttribute('data-slotid', input[1]);

						if(input[2] != undefined)
							slotElement.setAttribute('data-value', input[2]);

						let knot = document.createElement('em');
						slotElement.appendChild(knot);

						let slotName = document.createElement('span');
						slotName.innerHTML = input[0];
						slotElement.appendChild(slotName);

						let value = input[3]['m_Value'];

						if(value != undefined)
						{
							let inputSlot = document.createElement('div');
							inputSlot.className = 'input-slot';

							var html = '';

							if(typeof value == 'object')
							{
								let labels = ['X', 'Y', 'Z', 'W'];
								var dimensions = Object.keys(value).length;

								if(dimensions > 3)
								{
									var initialValue = undefined;
									var allValuesEqual = true;

									dimensions = 1;

									for(i in value)
									{
										if(initialValue == undefined)
											initialValue = value[i];
										else if(initialValue != value[i])
										{
											allValuesEqual = false;
											dimensions += 1;
											break;
										}
									}

									if(allValuesEqual)
									{
										inputSlot.innerHTML = labels[0] + ': <span>' + this.normalizedSlotValue(initialValue) + '</span>';
										dimensions = 1;
									}
								}

								var index = 0;

								for(i in value)
								{
									if(index > dimensions - 1)
										break;

									html += labels[index] + ': <span>' + this.normalizedSlotValue(value[i]) + '</span> ';
									index += 1;
								}
							}
							else
							{
								html += 'X: <span>' + this.normalizedSlotValue(value) + '</span>';
							}

							inputSlot.innerHTML = html;

							slotElement.appendChild(inputSlot);
						}

						slotInputs.appendChild(slotElement);

						this.edgeTable[guid]['i'][input[1]] = knot;
					});
				}

				if(outputs.length > 0)
				{
					let slotOutputs = document.createElement('ul');
					slotOutputs.className = 'slot-outputs';
					slotContainer.appendChild(slotOutputs);

					outputs.forEach((output) => {

						let slotElement = document.createElement('li');
						slotElement.setAttribute('data-slotid', output[1]);

						if(output[2] != undefined)
							slotElement.setAttribute('data-value', output[2]);

						let slotName = document.createElement('span');
						slotName.innerHTML = output[0];
						slotElement.appendChild(slotName);

						let knot = document.createElement('em');
						slotElement.appendChild(knot);

						slotOutputs.appendChild(slotElement);

						this.edgeTable[guid]['o'][output[1]] = knot;
					});
				}

				this.containerElement.appendChild(nodeElement);
			});

			this.svgElement = document.createElementNS("http://www.w3.org/2000/svg", "svg");
			this.svgElement.setAttribute('version', '1.1');
			this.svgElement.setAttribute('xmlns', 'http://www.w3.org/1999/xhtml');
			this.containerElement.appendChild(this.svgElement);

			this.svgElement.setAttribute('width', this.canvasSize.width);
			this.svgElement.setAttribute('height', this.canvasSize.height);

			this.svgElement.innerHTML = '<defs></defs>';

			let edges = serializedGraph['m_SerializableEdges'];

			if(edges != undefined
				&& edges.length > 0)
			{
				console.log('Calculating node slot dimensions...');

				var outputNodes = [];
				var inputNodes = [];

				edges.forEach((edge) => {

					let edgeData = edge['JSONnodeData'];
					let serializedEdge = JSON.parse(edgeData);

					let inputSlot = serializedEdge['m_InputSlot'];
					let inputGuid = inputSlot['m_NodeGUIDSerialized'];

					let outputSlot = serializedEdge['m_OutputSlot'];
					let outputGuid = outputSlot['m_NodeGUIDSerialized'];

					if(this.edgeElements[inputGuid] == undefined)
						this.edgeElements[inputGuid] = [];

					if(this.edgeElements[outputGuid] == undefined)
						this.edgeElements[outputGuid] = [];

					this.edgeElements[inputGuid].push(serializedEdge);
					this.edgeElements[outputGuid].push(serializedEdge);

					if(!outputNodes.includes(outputGuid))
						outputNodes.push(outputGuid);

					if(!inputNodes.includes(inputGuid))
						inputNodes.push(inputGuid);
				});

				// Nodes with no inputs but with filled outputs
				if (this.debugEnableSlotDimensions)
				{
					var startingNodes = [];

					outputNodes.forEach((outputGuid) => {

						if(!inputNodes.includes(outputGuid))
							startingNodes.push(outputGuid);
					});

					startingNodes.forEach((guid) => {
						this.recursivelyUpdateEdges(guid);
					});
				}

				console.log('Drawing edges...');

				this.drawEdges();
			}

			if(stickyNotes != undefined
				&& stickyNotes.length > 0)
			{
				console.log('Drawing sticky notes...');

				stickyNotes.forEach((stickyNote) => {

					let stickyPosition = stickyNote['m_Position'];
					let stickyGuid = stickyNote['m_GuidSerialized'];
					let groupGuid = stickyNote['m_GroupGuidSerialized'];

					if(this.groupTable[groupGuid] == undefined)
						this.groupTable[groupGuid] = [];

					this.groupTable[groupGuid].push(stickyGuid);

					let stickyTitle = stickyNote['m_Title'];
					let stickyContents = '<h5>' + stickyTitle + '</h5>';
					stickyContents += '<p>' + stickyNote['m_Content'] + '</p>';

					let stickyElement = document.createElement('div');
					stickyElement.className = "sticky";
					stickyElement.setAttribute('id', stickyGuid);
					stickyElement.innerHTML = stickyContents;
					stickyElement.dataset['groupguid'] = groupGuid;

					if(!this.isEmbedded)
					{
						let self = this;

						stickyElement.onmousedown = function(event) {
							self.dragStartCallback(event, this);
						}
						stickyElement.onmouseenter = function(event) {
							this.dataset['hovering'] = true;
							document.body.dataset['hovering'] = 1;
						}
						stickyElement.onmouseleave = function(event) {
							this.dataset['hovering'] = false;
							document.body.dataset['hovering'] = 0;
						}
					}

					let nodePositionX = stickyPosition.x - startX;
					let nodePositionY = stickyPosition.y - startY;

					stickyElement.style.top = nodePositionY + 'px';
					stickyElement.style.left = nodePositionX + 'px';
					stickyElement.style.minWidth = '200px';
					stickyElement.style.minHeight = '200px';

					this.containerElement.appendChild(stickyElement);
				});
			}

			if(groups != undefined
				&& groups.length > 0)
			{
				console.log('Drawing groups...');

				groups.forEach((serializedGroup) => {

					let groupTitle = serializedGroup['m_Title'];

					let groupPosition = serializedGroup['m_Position'];
					let groupGuid = serializedGroup['m_GuidSerialized'];

					let groupElement = document.createElement('div');
					groupElement.className = "group";
					groupElement.setAttribute('title', groupTitle);
					groupElement.setAttribute('id', groupGuid);

					let nodePositionX = groupPosition.x - startX;
					let nodePositionY = groupPosition.y - startY;

					groupElement.style.top = nodePositionY + 'px';
					groupElement.style.left = nodePositionX + 'px';
					groupElement.style.minWidth = '200px';
					groupElement.style.minHeight = '200px';

					let headerElement = document.createElement('h5');
					headerElement.innerText = groupTitle;
					groupElement.appendChild(headerElement);

					this.containerElement.appendChild(groupElement);

					if(!this.isEmbedded)
					{

						let self = this;

						headerElement.onmousedown = function(event) {
							self.dragStartCallback(event, this.parentNode);
						}
						headerElement.onmouseenter = function(event) {
							this.parentNode.dataset['hovering'] = true;
							document.body.dataset['hovering'] = 1;
						}
						headerElement.onmouseleave = function(event) {
							this.parentNode.dataset['hovering'] = false;
							document.body.dataset['hovering'] = 0;
						}
					}

					this.resizeGroup(groupElement);
				});
			}

			let leftMostElement = document.getElementById(leftMostStruct['guid']);
			let leftMostElementLeft = parseInt(leftMostElement.style.left);
			let leftMostElementTop = parseInt(leftMostElement.style.top);
			let leftMostElementWidth = leftMostElement.offsetWidth;
			let leftMostElementHeight= leftMostElement.offsetHeight;

			return {
				x: leftMostElementLeft + (leftMostElementWidth / 2),
				y: leftMostElementTop + (leftMostElementHeight / 2),
			}
		},

		normalizedSlotValue : function(value)
		{
			return (value == 0 ? 0 : parseFloat(value).toFixed(2));
		},

		recursivelyUpdateEdges : function(nodeGUID)
		{
			let edgeEntries = this.edgeElements[nodeGUID];

			if(edgeEntries == undefined)
				return;

			edgeEntries.forEach((serializedEdge) => {

				let inputSlot = serializedEdge['m_InputSlot'];
				let inputGuid = inputSlot['m_NodeGUIDSerialized'];
				let inputSlotId = inputSlot['m_SlotId'];

				let outputSlot = serializedEdge['m_OutputSlot'];
				let outputGuid = outputSlot['m_NodeGUIDSerialized'];
				let outputSlotId = outputSlot['m_SlotId'];

				// We only care about edge connections that the
				// current node (nodeGUID) is outputing
				if(outputGuid != nodeGUID)
					return;

				this.updateSlotNumberFromNode(outputGuid, outputSlotId,
											inputGuid, inputSlotId);

				// Repeat process for the next node
				this.recursivelyUpdateEdges(inputGuid);
			});
		},

		drawEdges : function()
		{
			for(let guid in this.edgeElements) {

				this.edgeElements[guid].forEach((serializedEdge) => {
					this.updateOrCreateEdge(serializedEdge);
				});
			}
		},

		updateSlotNumberFromNode : function(outputNodeGUID, outputSlotID, inputNodeGUID, inputSlotID)
		{
			let outputSlotNumberElement = document.getElementById(outputNodeGUID).querySelector('.slot-outputs li[data-slotid="' + outputSlotID + '"] i');

			if(outputSlotNumberElement == undefined)
				return;

			let slotNumber = outputSlotNumberElement.innerText;

			let inputNode = document.getElementById(inputNodeGUID);
			let inputFullName = inputNode.dataset['fullname'];

			// Don't change the input of certain nodes
			if(inputFullName == this.blendNodeType
				|| inputFullName == this.gradientNodeType
				|| inputFullName == this.unlitMasterNodeType
				|| inputFullName == this.gradientNoiseNodeType
				|| inputFullName == this.remapNodeType)
				return;

			let inputSlotNumber = inputNode.querySelector('.slot-inputs li[data-slotid="' + inputSlotID + '"] i');

			if(inputSlotNumber == undefined)
				return;
			let slotInputNumbers = inputNode.querySelectorAll('.slot-inputs li i');
			let outputSlotNumbers = inputNode.querySelectorAll('.slot-outputs li i');

			if(inputFullName == this.multiplyNodeType
				&& inputNode.dataset['assigned'] != undefined)
			{
				var maxValue = parseInt(inputNode.dataset['assigned']);

				if(slotNumber > maxValue)
					maxValue = slotNumber;

				inputNode.dataset['assigned'] = maxValue;
				slotNumber = maxValue;
				slotInputNumbers.forEach((slotNumberElement) => {
					this.updateSlotNumberValue(slotNumberElement, maxValue);
				});
			}
			else
			{
				this.updateSlotNumberValue(inputSlotNumber, slotNumber);
				inputNode.dataset['assigned'] = slotNumber;
			}

			// Don't change the output of certain nodes
			if(inputFullName == this.vector3NodeType
				|| inputFullName == this.randomRangeNodeType)
				return;

			if(outputSlotNumbers == undefined
				|| outputSlotNumbers.length == 0)
				return;

			if(inputFullName == this.multiplyNodeType)
			{
				this.updateSlotNumberValue(outputSlotNumbers[0], slotNumber);
			}
			else if(inputFullName == this.divideNodeType
				|| inputFullName == this.addNodeType
				|| inputFullName == this.subtractNodeType)
			{
				var minValue = 1;

				slotInputNumbers.forEach((slotNumberElement) => {
					let value = parseInt(slotNumberElement.innerText);

					if(value < minValue)
						minValue = value;
				});

				this.updateSlotNumberValue(outputSlotNumbers[0], minValue);
			}
			else
			{
				if(outputSlotNumbers.length > 1)
					return;

				outputSlotNumbers.forEach((slotNumberElement) => {
					this.updateSlotNumberValue(slotNumberElement, slotNumber);
				});
			}
		},

		updateSlotNumberValue(slotElement, value)
		{
			slotElement.innerText = value;
			slotElement.parentNode.parentNode.dataset['value'] = value;
		},

		resizeGroup : function(groupElement)
		{
			let groupGuid = groupElement.id
			var minLeft = 0;
			var minTop = 0;

			this.groupTable[groupGuid].forEach((nodeGUID) => {
				let nodeElement = document.getElementById(nodeGUID);

				let nodeLeft = parseInt(nodeElement.style.left);
				let nodeTop = parseInt(nodeElement.style.top);
				let nodeWidth = nodeElement.offsetWidth;
				let nodeHeight= nodeElement.offsetHeight;

				if(minLeft == 0 || minLeft > nodeLeft)
					minLeft = nodeLeft;

				if(minTop == 0 || minTop > nodeTop)
					minTop = nodeTop;
			});

			let padding = 40;

			minLeft -= padding;
			minTop -= padding;

			var groupWidth = 0;
			var groupHeight= 0;

			this.groupTable[groupGuid].forEach((nodeGUID) => {
				let nodeElement = document.getElementById(nodeGUID);

				let nodeLeft = parseInt(nodeElement.style.left);
				let nodeTop = parseInt(nodeElement.style.top);
				let nodeWidth = nodeElement.offsetWidth;
				let nodeHeight= nodeElement.offsetHeight;

				let relativeLeft = nodeLeft - minLeft;
				let relativeTop = nodeTop - minTop;

				if(groupWidth < relativeLeft + nodeWidth)
					groupWidth = relativeLeft + nodeWidth;

				if(groupHeight < relativeTop + nodeHeight)
					groupHeight = relativeTop + nodeHeight;
			});

			groupElement.style.top = minTop + 'px';
			groupElement.style.left = minLeft + 'px';
			groupElement.style.width = (groupWidth + padding) + 'px';
			groupElement.style.height = (groupHeight + padding) + 'px';
		}
	};

	window.ShaderGraphExtractor = ShaderGraphExtractor;
	window.PannerDragger = PannerDragger;
	window.FileLoader = FileLoader;
	window.Blackboard = Blackboard;
	window.Inspector = Inspector;
	window.CryptoSuite = CryptoSuite;
	window.OverlaidForm = OverlaidForm;
})();

document.addEventListener('DOMContentLoaded', () => {

	let uploaderButton = document.getElementById('uploader-button');
	let uploaderElement = document.getElementById('uploader');
	let containerElement = document.querySelector('.container');
	let blackboardElement = document.getElementById('blackboard-container');
	let blackboardButton = document.getElementById('blackboard');
	let inspectorElement = document.getElementById('inspector');
	let zoomElement = document.getElementById('zoom');

	let cryptoSuite = CryptoSuite.initialize();
	let inspector = Inspector.initialize(inspectorElement);
	let blackboard = Blackboard.initialize(blackboardElement, blackboardButton);
	let pannerDragger = PannerDragger.initialize(containerElement, zoomElement, blackboard, inspector);

	let embedder = OverlaidForm.initialize(
		document.getElementById('embed'),
		document.getElementById('embed-container'),
		() => {
			document.body.dataset['presenting'] = '0';
			pannerDragger.isPresenting = false;
		}
	);

	window.fileLoader = FileLoader.initialize(
		uploaderButton,
		uploaderElement,
		containerElement,
		blackboard,
		pannerDragger,
		inspector,
		cryptoSuite,
		embedder);
});