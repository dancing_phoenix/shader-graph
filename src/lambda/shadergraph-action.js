'use strict';

var request = require("request");

// populate environment variables locally.
require('dotenv').config()


/*
  delete this submission via the api
*/
function purgeComment(id) {
  var url = `https://api.netlify.com/api/v1/submissions/${id}?access_token=${process.env.API_AUTH}`;
  request.delete(url, function(err, response, body){
    if(err){
      return console.log(err);
    } else {
      return console.log("Shadergraph deleted.");
    }
  });
}

export function handler(event, context, callback) {

  var bodyComponents = event.body.split("payload=");

  var body = bodyComponents[1];
  var payload = JSON.parse(unescape(body));

  if(payload.actions == undefined)
  {
    callback(null, {
      statusCode: 403,
      body: 'Forbidden'
    })
    return;
  }

  var method = payload.actions[0].name
  var id = payload.actions[0].value

  if(method == "delete") {
    console.log("Purging shadergraph " + id + "...")
    purgeComment(id);
    callback(null, {
      statusCode: 200,
      body: "Shadergraph deleted"
    });
  } 
  else if (method == "approve")
  {
    console.log("Deploying...");

    request.post(`https://api.netlify.com/build_hooks/${process.env.DEPLOY_HOOK}`, function(err, httpResponse, body) {
      var msg;
      if (err) {
        msg = 'Deploying failed:' + err;
      } else {
        msg = 'Deploying was successful.'
      }
        console.log(msg);
      callback(null, {
        statusCode: 200,
        body: msg
      })
      return console.log(msg);
    });
  }    
  else if(method == 'extractPreview')
  {
    console.log("Extracting preview...");
    var url = `https://api.netlify.com/api/v1/submissions/${id}?access_token=${process.env.API_AUTH}`;

    request.get(url, function(err, response, body) {
      if(err) 
      {
        callback(null, {
          statusCode: 403,
          body: 'Error'
        })
        return console.log(err);
      }
      else if(response.statusCode != 200)
      {
        callback(null, {
          statusCode: response.statusCode,
          body: 'Error'
        })
        return console.log(response);
      }
      else {

        var serializedBody = JSON.parse(unescape(body));
        var encrypted = serializedBody.data.shadergraph;

        callback(null, {
          statusCode: 200,
          body: encrypted
        })
        return console.log('Preview found!');
      }
    });
  }
}
